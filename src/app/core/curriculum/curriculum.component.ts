import {Component, OnInit} from '@angular/core';
import {fadeOutOnLeaveAnimation, flashOnEnterAnimation} from "angular-animations";

@Component({
  selector: 'maple-curriculum',
  templateUrl: './curriculum.component.html',
  animations: [
    fadeOutOnLeaveAnimation(),
    flashOnEnterAnimation({delay:2000})
  ],
  styleUrls: ['./curriculum.component.scss']
})
export class CurriculumComponent implements OnInit {

  state = true;

  constructor() {
  }

  ngOnInit() {
    setTimeout(() =>{
      // document.getElementById('arrow-indicate').classList.add('hideur');
      this.state = false;
    },8000)
  }
}
