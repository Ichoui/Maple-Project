import {Component} from '@angular/core';
import {Jump} from "../../../providers/jump/jump";
import {JumpService} from "../../../providers/jump/jump.service";

@Component({
  selector: 'maple-psa',
  templateUrl: './psa.component.html',
  styleUrls: ['../jump.component.scss']
})
export class PsaComponent {

  public jump$: Jump;

  constructor(public jumpService: JumpService) {

    this.jumpService.getPSA().subscribe(i => {
      this.jump$ = i;
    });
  }
}
