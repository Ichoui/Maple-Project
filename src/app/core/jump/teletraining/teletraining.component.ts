import {Component} from '@angular/core';
import {Jump} from "../../../providers/jump/jump";
import {JumpService} from "../../../providers/jump/jump.service";

@Component({
  selector: 'maple-teletraining',
  templateUrl: './teletraining.component.html',
  styleUrls: ['../jump.component.scss']
})
export class TeletrainingComponent {

  public jump$: Jump;

  constructor(public jumpService: JumpService) {

    this.jumpService.getTTv2().subscribe(i => {
      this.jump$ = i;
    });
  }

}
