import {Component} from '@angular/core';
import {Jump} from "../../../providers/jump/jump";
import {JumpService} from "../../../providers/jump/jump.service";

@Component({
  selector: 'maple-mooc',
  templateUrl: './mooc.component.html',
  styleUrls: ['../jump.component.scss']
})
export class MoocComponent {

  public jump$: Jump;

  constructor(public jumpService: JumpService) {

    this.jumpService.getMOOC().subscribe(i => {
      this.jump$ = i;
    });
  }

}
