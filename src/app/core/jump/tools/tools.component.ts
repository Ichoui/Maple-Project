import {JumpService} from '../../../providers/jump/jump.service';
import {Jump} from '../../../providers/jump/jump';
import {Component} from '@angular/core';


@Component({
  selector: 'maple-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['../jump.component.scss']
})

export class ToolsComponent {

  public jump$: Jump;

  constructor(public jumpService: JumpService) {

    this.jumpService.getTools().subscribe(i => {
      this.jump$ = i;
    });
  }
}
